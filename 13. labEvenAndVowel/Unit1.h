//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiMain;
	TTabItem *tiPlay;
	TTabItem *tiFinish;
	TTabItem *tiHelp;
	TLayout *Layout1;
	TLabel *Label1;
	TLabel *Label2;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buPlay;
	TButton *buHelp;
	TButton *buExit;
	TToolBar *ToolBar1;
	TButton *buBack;
	TLabel *laTime;
	TLabel *laScore;
	TTimer *tmPlay;
	TGridPanelLayout *GridPanelLayout2;
	TButton *buYes;
	TButton *buNo;
	TGridPanelLayout *GridPanelLayout3;
	TLabel *Label3;
	TRectangle *Rectangle1;
	TLabel *laEven;
	TLabel *Label4;
	TRectangle *Rectangle2;
	TLabel *laVowel;
	TLayout *Layout2;
	TLabel *Label5;
	TLabel *laFinishCorrect;
	TLabel *laFinishWrong;
	TGridPanelLayout *GridPanelLayout4;
	TButton *buReplay;
	TButton *buMenu;
	TToolBar *ToolBar2;
	TButton *buBack2;
	TLabel *Label7;
	TLayout *Layout3;
	TLabel *Label9;
	TStyleBook *StyleBook1;
	TLabel *Label6;
	void __fastcall buExitClick(TObject *Sender);
	void __fastcall buBackClick(TObject *Sender);
	void __fastcall buPlayClick(TObject *Sender);
	void __fastcall buHelpClick(TObject *Sender);
	void __fastcall tmPlayTimer(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buReplayClick(TObject *Sender);
private:	// User declarations
	int FCountCorrect;
	int FCountWrong;
	bool FAnswerCorrect;
	double FTimeValue;
	void DoReset();
	void DoContinue();
	void DoAnswer(bool aValue);
	void DoFinish();
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
