//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buExitClick(TObject *Sender)
{
    Close();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buBackClick(TObject *Sender)
{
    tc->ActiveTab = tiMain;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buPlayClick(TObject *Sender)
{
	DoReset();
	tc->ActiveTab = tiPlay;

}
//---------------------------------------------------------------------------

void __fastcall TForm1::buHelpClick(TObject *Sender)
{
    tc->ActiveTab = tiHelp;
}
//---------------------------------------------------------------------------

void TForm1::DoReset(){
	FCountCorrect = 0;
	FCountWrong = 0;
	FTimeValue = Time().Val + (float)30/(24*60*60);
	tmPlay->Enabled = true;
	DoContinue();
}

void TForm1::DoContinue(){
     // TODo
}

void TForm1::DoAnswer(bool aValue){
	(aValue == FAnswerCorrect) ? FCountCorrect++ : FCountWrong++;
	DoContinue();
}


void TForm1::DoFinish(){
	tmPlay->Enabled = false;
	laFinishCorrect->Text = Format(L"Правильно: %d", ARRAYOFCONST((FCountCorrect)));
	laFinishWrong->Text = Format(L"Неправильно: %d", ARRAYOFCONST((FCountWrong)));
	tc->ActiveTab = tiFinish;
}
void __fastcall TForm1::tmPlayTimer(TObject *Sender)
{
	double x = FTimeValue - Time().Val;
	laTime->Text = FormatDateTime("nn:ss", x);
	if(x <= 0)
        DoFinish();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormCreate(TObject *Sender)
{
	tc->ActiveTab = tiMain;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buReplayClick(TObject *Sender)
{
	DoReset();
    tc->ActiveTab = tiPlay;
}
//---------------------------------------------------------------------------

