//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TToolBar *tbTop;
	TToolBar *tbItemBottom;
	TTabControl *tc;
	TTabItem *Tab1;
	TTabItem *Tab2;
	TListView *lv;
	TStyleBook *StyleBook1;
	TLabel *Label1;
	TEdit *edCaption;
	TLayout *ly;
	TLabel *Label2;
	TTrackBar *tbPropitry;
	TLabel *Label3;
	TMemo *meDetail;
	TButton *buSave;
	TButton *buCansel;
	TButton *buDelete;
	TLabel *laName;
	TButton *buAbout;
	TToolBar *tbListBottom;
	TButton *buAdd;
	TBindingsList *BindingsList1;
	TBindSourceDB *BindSourceDB1;
	TBindSourceDB *BindSourceDB2;
	TLinkControlToField *LinkControlToField1;
	TLinkControlToField *LinkControlToField2;
	TLinkControlToField *LinkControlToField3;
	TLinkListControlToField *LinkListControlToField1;
	TTabItem *Tab3;
	TLabel *Label4;
	TLabel *Label5;
	TButton *buPrev;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall buAddClick(TObject *Sender);
	void __fastcall lvItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall buSaveClick(TObject *Sender);
	void __fastcall buCanselClick(TObject *Sender);
	void __fastcall buDeleteClick(TObject *Sender);
	void __fastcall buAboutClick(TObject *Sender);
	void __fastcall buPrevClick(TObject *Sender);

private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
