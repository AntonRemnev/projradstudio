object dm: Tdm
  OldCreateOrder = False
  Height = 227
  Width = 330
  object FDConnection: TFDConnection
    Params.Strings = (
      'Database=Notes.db'
      'DriverID=SQLite')
    LoginPrompt = False
    AfterConnect = FDConnectionAfterConnect
    BeforeConnect = FDConnectionBeforeConnect
    Left = 40
    Top = 72
  end
  object taNotes: TFDTable
    Connection = FDConnection
    UpdateOptions.UpdateTableName = 'Notes'
    TableName = 'Notes'
    Left = 216
    Top = 56
    object taNotesCaption: TStringField
      FieldName = 'Caption'
      Origin = 'Caption'
      Required = True
      Size = 50
    end
    object taNotesPriority: TSmallintField
      FieldName = 'Priority'
      Origin = 'Priority'
      Required = True
    end
    object taNotesDetail: TStringField
      FieldName = 'Detail'
      Origin = 'Detail'
      Size = 500
    end
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 168
    Top = 152
  end
  object FDPhysSQLiteDriverLink1: TFDPhysSQLiteDriverLink
    Left = 248
    Top = 152
  end
end
