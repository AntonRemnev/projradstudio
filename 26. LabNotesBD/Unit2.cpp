//---------------------------------------------------------------------------


#pragma hdrstop

#include "Unit2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
Tdm *dm;
//---------------------------------------------------------------------------
__fastcall Tdm::Tdm(TComponent* Owner)
	: TDataModule(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tdm::FDConnectionAfterConnect(TObject *Sender)
{
	FDConnection->ExecSQL(
	" CREATE TABLE IF NOT EXISTS [NOTES]("
	" [Caption] VARCHAR(50) NOT NULL,"
	" [Priority] SMALLINT NOT NULL,"
	" [Detail] VARCHAR(500))"
	);
	taNotes->Open();
}
//---------------------------------------------------------------------------

void __fastcall Tdm::FDConnectionBeforeConnect(TObject *Sender)
{
	FDConnection->Params->Values["Database"]=
#ifdef _Windows
		"..\\..\\"+ cNameDB;
#else
		System::Ioutils::GetDocumentsPath() +PathDelim + cNameDB;
#endif
}
//---------------------------------------------------------------------------

