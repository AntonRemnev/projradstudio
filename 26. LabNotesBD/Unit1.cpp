//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
#include "Unit2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormCreate(TObject *Sender)
{
	tc->ActiveTab=Tab1;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormShow(TObject *Sender)
{
    dm->FDConnection->Connected=true;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buAddClick(TObject *Sender)
{
	dm->taNotes->Append();
	tc->GotoVisibleTab(Tab2->Index);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::lvItemClick(TObject * const Sender, TListViewItem * const AItem)

{
	dm->taNotes->Edit();
	tc->GotoVisibleTab(Tab2->Index);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buSaveClick(TObject *Sender)
{
	dm->taNotes->Post();
	tc->GotoVisibleTab(Tab1->Index);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buCanselClick(TObject *Sender)
{
	dm->taNotes->Cancel();
	tc->GotoVisibleTab(Tab1->Index);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buDeleteClick(TObject *Sender)
{
	dm->taNotes->Delete();
	tc->GotoVisibleTab(Tab1->Index);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buAboutClick(TObject *Sender)
{
	tc->ActiveTab = Tab3;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buPrevClick(TObject *Sender)
{
	tc->ActiveTab = Tab1;
}
//---------------------------------------------------------------------------

