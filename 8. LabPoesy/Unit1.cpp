//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buMinusClick(TObject *Sender)
{
	meTextFull->TextSettings->Font->Size -=2;
	meTextRow->TextSettings->Font->Size -=2;
	meTextWords->TextSettings->Font->Size -=2;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buPlusClick(TObject *Sender)
{
	meTextFull->TextSettings->Font->Size +=2;
	meTextRow->TextSettings->Font->Size +=2;
	meTextWords->TextSettings->Font->Size +=2;
}
inline int Low(const System::UnicodeString &)
{
	#ifdef _DELPHI_STRING_ONE_BASED
	return 1;
	#else
	return 0;
	#endif
}
//-------------------------------------------------------------------------�
inline int High(const System::UnicodeString &S)
{
	#ifdef _DELPHI_STRING_ONE_BASED
	return S.Length();
	#else
	return S.Leng��() - 1;
	#endif
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
	System::UnicodeString x;

	for (int i= 1; i < meTextFull->Lines->Count; i++)
	{
		x = meTextFull->Lines->Strings[i];
	   if (i % 2 == 1 )
	   {
		for (int j = Low(x); j <= High(x); j++)
		{
			if (x[j] != ' ') x[j] = 'x';
		}
	   }
	   meTextRow->Lines->Add(x);
	}

	bool xFlag;
	for (int i = 0; i < meTextFull->Lines->Count; i++)
	{
		x = meTextFull->Lines->Strings[i];
		xFlag = false;
		for (int j = Low(x); j <= High(x); j++)
		{
			if((xFlag) && (x[j] != ' '))
			{
				x[j] = 'x';
			}

			if((!xFlag) && (x[j] == ' '))
			{
				xFlag = true;
			}
			}
		meTextWords->Lines->Add(x);
	}
}
//---------------------------------------------------------------------------
