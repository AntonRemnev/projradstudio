//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Effects.hpp>
#include <FMX.Filter.Effects.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TImage *Image1;
	TImage *Image2;
	TImage *Image3;
	TImage *Image4;
	TImage *Image5;
	TImage *Image6;
	TButton *Button1;
	TButton *Button2;
	TButton *Button3;
	TButton *Button4;
	TButton *Button5;
	TButton *Button6;
	TReflectionEffect *ReflectionEffect1;
	TBlurEffect *BlurEffect1;
	TShadowEffect *ShadowEffect1;
	TMaskToAlphaEffect *MaskToAlphaEffect1;
	TInvertEffect *InvertEffect1;
	TContrastEffect *ContrastEffect1;
	TMagnifyEffect *MagnifyEffect1;
	TShadowEffect *ShadowEffect2;
	TGlowEffect *GlowEffect1;
	TBlurEffect *BlurEffect2;
	TInnerGlowEffect *InnerGlowEffect1;
	TBlurEffect *BlurEffect3;
	TBlurEffect *BlurEffect4;
	TStyleBook *StyleBook1;
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
