//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
#include "Unit2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

	void TForm1::ReSetSelection(TObject *Sender)
	{
	FSel = dynamic_cast<TSelection*>(Sender);
	TSelection* x;
	for (int i = 0; i < ly->ComponentCount; i++) {
	x = dynamic_cast<TSelection*>(ly->Components[i]);
		if (x) {
				x->HideSelection = (x != FSel);
		}
	
	}
		tbOption->Visible = (FSel != NULL);
		if (tbOption->Visible)
		{
			tbRotation->Value = FSel->RotationAngle;
		}

	}

void __fastcall TForm1::Selection1MouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, float X, float Y)
{
    ReSetSelection(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button1Click(TObject *Sender)
{
	TSelection *x = new TSelection(ly);
	x->Parent = ly;
	x->Width = 50 + Random(100);
	x->Height = 50 + Random(100);
	x->Position->X = Random(ly->Width - x->Width);
	x->Position->Y = Random(ly->Height - x->Height);
	x->RotationAngle = RandomRange(-40,40);
	x->OnMouseDown = Selection1MouseDown;
	TGlyph *xGlyph = new TGlyph(x);
	xGlyph->Parent = x;
	xGlyph->Align = TAlignLayout::Client;
	xGlyph->Images = dm->il;
	xGlyph->ImageIndex = Random(dm->il->Count);

	ReSetSelection(x);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormCreate(TObject *Sender)
{
	ReSetSelection(Sender);
		
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button2Click(TObject *Sender)
{
	for (int i =ly->ComponentCount-1; i >=0; i--) {
		if(dynamic_cast<TSelection*>(ly->Components[i])) {
			ly->RemoveObject(i); 
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::lyMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y)
{
			  ReSetSelection(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button4Click(TObject *Sender)
{
	TGlyph *x = (TGlyph*)FSel->Components[0];
	x->ImageIndex = ((int) x->ImageIndex >= dm->il->Count - 1) ? 0 : (int)x->ImageIndex + 1;

}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button3Click(TObject *Sender)
{
	TGlyph *x =(TGlyph*)FSel->Components[0];
			x->ImageIndex = ((int) x->ImageIndex <= 0) ? dm->il->Count - 1 : (int)x->ImageIndex - 1;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::tbRotationChange(TObject *Sender)
{
	FSel->RotationAngle = tbRotation->Value;	
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button7Click(TObject *Sender)
{
	FSel->DisposeOf();
	FSel = NULL;
	ReSetSelection(FSel);	
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button6Click(TObject *Sender)
{
FSel->BringToFront();
FSel->Repaint(); 	
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button5Click(TObject *Sender)
{
	FSel->SendToBack() ;
	FSel->Repaint();	
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button8Click(TObject *Sender)
{
	ShowMessage("Повалишина Анастасия 151-364");
}
//---------------------------------------------------------------------------

