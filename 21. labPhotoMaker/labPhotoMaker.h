//---------------------------------------------------------------------------

#ifndef labPhotoMakerH
#define labPhotoMakerH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.ActnList.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.MediaLibrary.Actions.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdActns.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <System.Actions.hpp>
//---------------------------------------------------------------------------
class TForm2 : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *Button1;
	TButton *Button2;
	TButton *Button3;
	TButton *Button4;
	TImage *im;
	TActionList *ActionList1;
	TAction *ClearImg;
	TTakePhotoFromLibraryAction *TakePhotoFromLibraryAction1;
	TTakePhotoFromCameraAction *TakePhotoFromCameraAction1;
	TShowShareSheetAction *ShowShareSheetAction1;
	TStyleBook *StyleBook1;
	void __fastcall ClearImgExecute(TObject *Sender);
	void __fastcall TakePhotoFromLibraryAction1DidFinishTaking(TBitmap *Image);
	void __fastcall ShowShareSheetAction1BeforeExecute(TObject *Sender);
	void __fastcall TakePhotoFromCameraAction1DidFinishTaking(TBitmap *Image);

private:	// User declarations
public:		// User declarations
	__fastcall TForm2(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm2 *Form2;
//---------------------------------------------------------------------------
#endif
