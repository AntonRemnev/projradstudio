//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "labPhotoMaker.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm2 *Form2;
//---------------------------------------------------------------------------
__fastcall TForm2::TForm2(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm2::ClearImgExecute(TObject *Sender)
{
	//
    im->Bitmap->SetSize(0,0);
}
//---------------------------------------------------------------------------
void __fastcall TForm2::TakePhotoFromLibraryAction1DidFinishTaking(TBitmap *Image)

{
	//
    im->Bitmap->Assign(Image);
}
//---------------------------------------------------------------------------
void __fastcall TForm2::ShowShareSheetAction1BeforeExecute(TObject *Sender)
{
	//
	ShowShareSheetAction1->Bitmap->Assign(im->Bitmap);
}
//---------------------------------------------------------------------------
void __fastcall TForm2::TakePhotoFromCameraAction1DidFinishTaking(TBitmap *Image)

{
     im->Bitmap->Assign(Image);
}
//---------------------------------------------------------------------------
