// ---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Test.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;

// ---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner) : TForm(Owner) {
}
// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------

void __fastcall TForm1::FirstQuestion(TObject *Sender) {
	if (((TControl*) Sender)->Tag == 1)
		Memo1->Lines->Add("1 - �� ��������� �������");
	else
		Memo1->Lines->Add("1 - �� �����!!!");
	TabControl1->ActiveTab = TabItem2;

}
// ---------------------------------------------------------------------------

void __fastcall TForm1::SecondQuestion(TObject *Sender) {
	if (((TControl*) Sender)->Tag == 1)
		Memo1->Lines->Add("2 - �����");
	else
		Memo1->Lines->Add("2 - �������");
	TabControl1->ActiveTab = TabItem4;
}
// ---------------------------------------------------------------------------

void __fastcall TForm1::FourthQuestion(TObject *Sender) {
	if (((TControl*) Sender)->Tag == 1)
		Memo1->Lines->Add("3 - �����!");
	else
		Memo1->Lines->Add("");
    TabControl1->ActiveTab = TabItem5;
}
// ---------------------------------------------------------------------------

