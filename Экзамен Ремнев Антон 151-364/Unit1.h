//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Ani.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Colors.hpp>
#include <FMX.ListBox.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:
	TTabControl *tc;
	TTabItem *tiMenu;
	TTabItem *tiPlay;
	TTabItem *tiSettings;
	TTabItem *tiResult;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buPlay;
	TButton *buSettings;
	TButton *buHelp;
	TButton *buAbout;
	TLabel *Label1;
	TFloatAnimation *FloatAnimation1;
	TFloatAnimation *FloatAnimation2;
	TTabItem *tiHelp;
	TRectangle *ReBackground1;
	TToolBar *ToolBar1;
	TButton *buBack;
	TToolBar *ToolBar2;
	TButton *buBack2;
	TToolBar *ToolBar3;
	TButton *buBack3;
	TLabel *Label4;
	TLabel *Label5;
	TGridPanelLayout *GridPanelLayout3;
	TButton *buGood;
	TButton *buBad;
	TLabel *laGameTimer;
	TGridPanelLayout *GridPanelLayout4;
	TRectangle *ReColor;
	TLabel *laColor;
	TLabel *laStartTimer;
	TLabel *Label6;
	TTimer *TimerGameStart;
	TTimer *TimerGameLong;
	TLayout *Layout1;
	TLabel *laGood;
	TLabel *laBad;
	TButton *buPlayAgain;
	TButton *buGoMenu;
	TText *Text1;
	TRectangle *ReBackground2;
	TRectangle *ReBackground3;
	TRectangle *ReBackground4;
	TRectangle *ReBackground5;
	TStyleBook *StyleBook1;
	TLabel *Label2;
	TComboBox *cbGameTime;
	TColorComboBox *ColorComboBox1;
	TLabel *Label3;
	void __fastcall buAboutClick(TObject *Sender);
	void __fastcall buAllMouseEnter(TObject *Sender);
	void __fastcall buAllMouseLeave(TObject *Sender);
	void __fastcall buSettingsClick(TObject *Sender);
	void __fastcall buHelpClick(TObject *Sender);
	void __fastcall buPlayClick(TObject *Sender);
	void __fastcall buBackClick(TObject *Sender);
	void __fastcall buAnswerClick(TObject *Sender);
	void __fastcall cbGameTimeChange(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall ColorComboBox1Change(TObject *Sender);
	void __fastcall FormKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift);
	void __fastcall TimerGameStartTimer(TObject *Sender);
	void __fastcall TimerGameLongTimer(TObject *Sender);

private:
	int FGoodAnswers;
	int FBadAnswers;
	bool isCorrect;


	int FTimeToStart;
	int FGameTime;

	int SGameTime;

	void DoReset();
	void DoRandom();
    void DoChoise(int aTag);

public:
	__fastcall TForm1(TComponent* Owner);
};

const UnicodeString colorName[] = {L"�������", L"������", L"�������", L"�����"};

const TAlphaColor colorCode[] = {TAlphaColorRec::Red, TAlphaColorRec::Yellow, TAlphaColorRec::Green, TAlphaColorRec::Blue};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
