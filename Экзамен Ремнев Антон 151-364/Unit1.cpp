//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
  __fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buAboutClick(TObject *Sender)
{
	ShowMessage(L"��������� ���������� ������ ����� 151-364.");
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buAllMouseEnter(TObject *Sender)
{

   TButton *bu = (TButton*) Sender;
   bu->TextSettings->Font->Size += 10;
   bu->Margins->Rect = TRect(2,2,2,2);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buAllMouseLeave(TObject *Sender)
{

	TButton *bu = (TButton*) Sender;
	bu->TextSettings->Font->Size -=10;
	bu->Margins->Rect = TRect(1,1,1,1);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buSettingsClick(TObject *Sender)
{
    tc->GotoVisibleTab(tiSettings->Index);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buHelpClick(TObject *Sender)
{
    tc->GotoVisibleTab(tiHelp->Index);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buPlayClick(TObject *Sender)
{
	DoReset();
    tc->GotoVisibleTab(tiPlay->Index);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buBackClick(TObject *Sender)
{
    tc->GotoVisibleTab(tiMenu->Index);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buAnswerClick(TObject *Sender)
{
	TButton *bu = (TButton*) Sender;
	DoChoise(bu->Tag);

}
//---------------------------------------------------------------------------
void TForm1::DoReset(){
	FGoodAnswers = 0;
	FBadAnswers = 0;
	FTimeToStart = 3;
	FGameTime = SGameTime;

	laGameTimer->Text = FGameTime;

	laStartTimer->Text = L"�� ������ �������� �����: " + IntToStr(FTimeToStart);
	laStartTimer->Visible = true;
	TimerGameStart->Enabled = true;
	TimerGameLong->Enabled = false;
	buGood->Enabled = false;
	buBad->Enabled = false;
	laColor->Text = L"����";
	ReColor->Fill->Color = TAlphaColorRec::Azure;

}
void __fastcall TForm1::cbGameTimeChange(TObject *Sender)
{
   SGameTime = StrToInt(cbGameTime->Selected->Text);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormCreate(TObject *Sender)
{
    Randomize();
	SGameTime = 20;
	tc->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------


void __fastcall TForm1::ColorComboBox1Change(TObject *Sender)
{

	ReBackground1->Fill->Color = ColorComboBox1->Color;
	ReBackground2->Fill->Color = ColorComboBox1->Color;
	ReBackground3->Fill->Color = ColorComboBox1->Color;
	ReBackground4->Fill->Color = ColorComboBox1->Color;
	ReBackground5->Fill->Color = ColorComboBox1->Color;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift)
{

	if(tc->ActiveTab != tiMenu && Key == vkHardwareBack){
	   tc->GotoVisibleTab(tiMenu->Index);
       Key = 0;
    }
}
//---------------------------------------------------------------------------
void TForm1::DoRandom(){
	int r = Random(2);

	if(r == 1){
		int colorId = Random(4);
		laColor->Text = colorName[colorId];
		ReColor->Fill->Color = colorCode[colorId];
		isCorrect = true;
	}else{
		int colorId1 = Random(4);
		int colorId2 = Random(4);
		while(colorId1 == colorId2) colorId2 = Random(4);
		laColor->Text = colorName[colorId1];
		ReColor->Fill->Color = colorCode[colorId2];
		isCorrect = false;
	}
}

void TForm1::DoChoise(int aTag){
	 if(aTag == isCorrect) FGoodAnswers++;
	 else FBadAnswers++;
	 DoRandom();

}
void __fastcall TForm1::TimerGameStartTimer(TObject *Sender)
{

	FTimeToStart--;
	laStartTimer->Text = L"�� ������ �������� �����: " + IntToStr(FTimeToStart);
	if(FTimeToStart <= 0){
		DoRandom();
		laStartTimer->Visible = false;
		TimerGameLong->Enabled = true;
		TimerGameStart->Enabled = false;
		buBad->Enabled = true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::TimerGameLongTimer(TObject *Sender)
{

	FGameTime--;
	laGameTimer->Text = IntToStr(FGameTime);
	if(FGameTime <= 0){
		TimerGameLong->Enabled = false;
		laGood->Text = L"���������: " + IntToStr(FGoodAnswers);
		laBad->Text = L"�����������: " + IntToStr(FBadAnswers);
		tc->GotoVisibleTab(tiResult->Index);

	}
}
//---------------------------------------------------------------------------


