//---------------------------------------------------------------------------

#ifndef labCountBoxH
#define labCountBoxH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>

const int cMaxBox = 25;
const int cMaxAnswer = 6;
const int cMinPossible = 4;
const int cMaxPossible = 14;
//---------------------------------------------------------------------------
class TForm2 : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiMain;
	TTabItem *tiPlay;
	TTabItem *tiResult;
	TToolBar *ToolBar1;
	TButton *buReset;
	TLabel *Label1;
	TLabel *laTime;
	TLayout *Layout1;
	TLayout *Layout2;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buAnswer1;
	TButton *buAnswer2;
	TButton *buAnswer3;
	TButton *buAnswer4;
	TButton *buAnswer5;
	TButton *buAnswer6;
	TGridPanelLayout *GridPanelLayout2;
	TRectangle *Rectangle1;
	TRectangle *Rectangle2;
	TRectangle *Rectangle3;
	TRectangle *Rectangle4;
	TRectangle *Rectangle5;
	TRectangle *Rectangle6;
	TRectangle *Rectangle7;
	TRectangle *Rectangle8;
	TRectangle *Rectangle9;
	TRectangle *Rectangle10;
	TRectangle *Rectangle11;
	TRectangle *Rectangle12;
	TRectangle *Rectangle13;
	TRectangle *Rectangle14;
	TRectangle *Rectangle15;
	TRectangle *Rectangle16;
	TRectangle *Rectangle17;
	TRectangle *Rectangle18;
	TRectangle *Rectangle19;
	TRectangle *Rectangle20;
	TRectangle *Rectangle21;
	TRectangle *Rectangle22;
	TRectangle *Rectangle23;
	TRectangle *Rectangle24;
	TRectangle *Rectangle25;
	TTimer *tmPlay;
	TLayout *Layout3;
	TGridPanelLayout *GridPanelLayout3;
	TButton *buStartPlay;
	TButton *buRules;
	TLayout *Layout4;
	TLabel *laResult;
	TButton *buPlayAgain;
	TButton *buGoMenu;
	TTabItem *tiRules;
	TText *Text1;
	TToolBar *ToolBar2;
	TButton *buBack;
	TStyleBook *StyleBook1;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall buAnswerAllClick(TObject *Sender);
	void __fastcall buResetClick(TObject *Sender);
	void __fastcall tmPlayTimer(TObject *Sender);
	void __fastcall buStartPlayClick(TObject *Sender);
	void __fastcall buPlayAgainClick(TObject *Sender);
	void __fastcall buBackClick(TObject *Sender);
private:	// User declarations
	int FCountCorrect;
	int FCountWrong;
	int FNumberCorrect;
	double FTimeValue;
	TList *FListBox;
	TList *FListAnswer;
	void DoReset();
	void DoContinue();
	void DoAnswer(int aValue);
	void DoFinish();


public:		// User declarations
	__fastcall TForm2(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm2 *Form2;
//---------------------------------------------------------------------------
#endif
