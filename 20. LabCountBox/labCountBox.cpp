//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "labCountBox.h"
#include "uUtils.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm2 *Form2;


//---------------------------------------------------------------------------
__fastcall TForm2::TForm2(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm2::FormCreate(TObject *Sender)
{
    tc->ActiveTab=tiMain;
	FListBox = new TList;
	for(int i = 1; i <= cMaxBox; i++){
		FListBox->Add(this->FindComponent("Rectangle"+IntToStr(i)));
	}
	FListAnswer = new TList;
	FListAnswer->Add(buAnswer1);
	FListAnswer->Add(buAnswer2);
	FListAnswer->Add(buAnswer3);
	FListAnswer->Add(buAnswer4);
	FListAnswer->Add(buAnswer5);
	FListAnswer->Add(buAnswer6);
	Randomize();

}
//---------------------------------------------------------------------------

void __fastcall TForm2::FormDestroy(TObject *Sender)
{
	delete FListBox;
    delete FListAnswer;
}
//---------------------------------------------------------------------------
void TForm2::DoReset(){
	FCountCorrect = 0;
	FCountWrong = 0;
	FTimeValue = Time().Val + (double)30/(24*60*60);
	tmPlay->Enabled = true;
    DoContinue();
}


void TForm2::DoContinue(){
	for(int i = 0; i < cMaxBox; i++){
		((TRectangle*)FListBox->Items[i])->Fill->Color = TAlphaColorRec::Lightgray;
	}

	FNumberCorrect = RandomRange(cMinPossible, cMaxPossible);
	int *x = RandomArrayUnique(cMaxBox, FNumberCorrect);
	for(int i = 0; i < FNumberCorrect; i++){
        ((TRectangle*)FListBox->Items[x[i]])->Fill->Color = TAlphaColorRec::Lightblue;
	}

	int xAnswerStart = FNumberCorrect - Random(cMaxAnswer-1);
	if(xAnswerStart < cMinPossible)
		xAnswerStart = cMinPossible;

	for(int i = 0; i < cMaxAnswer; i++){
        ((TButton*)FListAnswer->Items[i])->Text = IntToStr(xAnswerStart + i);
	}
}


void TForm2::DoAnswer(int aValue){
	(aValue == FNumberCorrect) ? FCountCorrect++ : FCountWrong++;
	if(FCountWrong > 5){
        DoFinish();
		laResult->Text = L"�����, ����, �� �� �������� ������ 5 ���";

	}
    DoContinue();
}

void TForm2::DoFinish(){
	tmPlay->Enabled = false;
	tc->GotoVisibleTab(tiResult->Index);
	laResult->Text = "���������: " + IntToStr(FCountCorrect) + ". �����������: " + IntToStr(FCountWrong);
}


void __fastcall TForm2::buAnswerAllClick(TObject *Sender)
{
	DoAnswer(StrToInt(((TButton*)Sender)->Text));
}
//---------------------------------------------------------------------------

void __fastcall TForm2::buResetClick(TObject *Sender)
{
	DoReset();
}
//---------------------------------------------------------------------------

void __fastcall TForm2::tmPlayTimer(TObject *Sender)
{
	double x = FTimeValue - Time().Val;
	laTime->Text = FormatDateTime("nn:ss", x);
	if(x<=0)
        DoFinish();
}
//---------------------------------------------------------------------------

void __fastcall TForm2::buStartPlayClick(TObject *Sender)
{
    DoReset();
	tc->GotoVisibleTab(tiPlay->Index);

}
//---------------------------------------------------------------------------

void __fastcall TForm2::buPlayAgainClick(TObject *Sender)
{
	tc->GotoVisibleTab(tiPlay->Index);
    DoReset();
}
//---------------------------------------------------------------------------

void __fastcall TForm2::buBackClick(TObject *Sender)
{
	tc->GotoVisibleTab(tiMain->Index);
}
//---------------------------------------------------------------------------

