//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TMemo *MeLog;
	TButton *Bu1;
	TButton *Bu2;
	TButton *Bu3;
	TButton *Bu4;
	TSwitch *Sw;
	TRadioButton *R1;
	TCheckBox *CB1;
	void __fastcall Bu1Click(TObject *Sender);
	void __fastcall Bu2Click(TObject *Sender);
	void __fastcall Bu3Click(TObject *Sender);
	void __fastcall Bu4Click(TObject *Sender);
	void __fastcall SwSwitch(TObject *Sender);
	void __fastcall R1Change(TObject *Sender);
	void __fastcall CB1Change(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
