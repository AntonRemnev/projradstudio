//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Bu1Click(TObject *Sender)
{
	MeLog->Lines->Clear();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Bu2Click(TObject *Sender)
{
	MeLog->Lines->Add("Bu2.OnClick");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Bu3Click(TObject *Sender)
{
	MeLog->Lines->Add(Bu3->Name +".OnClick");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Bu4Click(TObject *Sender)
{
	MeLog->Lines->Add(((TControl *)Sender)->Name + ".OnClick");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::SwSwitch(TObject *Sender)
{
	MeLog->Lines->Add(((TControl *)Sender)->Name + ".OnClick");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::R1Change(TObject *Sender)
{
	MeLog->Lines->Add(((TControl *)Sender)->Name + ".OnClick");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::CB1Change(TObject *Sender)
{
	MeLog->Lines->Add(((TControl *)Sender)->Name + ".OnClick");
}
//---------------------------------------------------------------------------
