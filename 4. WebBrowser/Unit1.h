//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.WebBrowser.hpp>
//---------------------------------------------------------------------------
class TFm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TToolBar *ToolBar2;
	TEdit *ed;
	TButton *buBack;
	TButton *buStop;
	TButton *buInf;
	TButton *buRefresh;
	TButton *buNext;
	TButton *buGo;
	TWebBrowser *wb;
	TStyleBook *StyleBook1;
	void __fastcall buGoClick(TObject *Sender);
	void __fastcall buBackClick(TObject *Sender);
	void __fastcall buNextClick(TObject *Sender);
	void __fastcall buRefreshClick(TObject *Sender);
	void __fastcall buStopClick(TObject *Sender);
	void __fastcall buInfClick(TObject *Sender);
	void __fastcall edKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift);
	void __fastcall wbDidFinishLoad(TObject *ASender);
private:	// User declarations
public:		// User declarations
	__fastcall TFm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFm *Fm;
//---------------------------------------------------------------------------
#endif
