//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TFm *Fm;
//---------------------------------------------------------------------------
__fastcall TFm::TFm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void __fastcall TFm::buBackClick(TObject *Sender)
{
	wb->GoBack();
}
//---------------------------------------------------------------------------
void __fastcall TFm::buNextClick(TObject *Sender)
{
	wb->GoForward();
}
//---------------------------------------------------------------------------
void __fastcall TFm::buRefreshClick(TObject *Sender)
{
	wb->Reload();
}
//---------------------------------------------------------------------------
void __fastcall TFm::buStopClick(TObject *Sender)
{
	wb->Stop();
}
//---------------------------------------------------------------------------
void __fastcall TFm::buInfClick(TObject *Sender)
{
	ShowMessage("�����������: ������ �����(151-364)");
}
//---------------------------------------------------------------------------
 void __fastcall TFm::buGoClick(TObject *Sender)
{
	wb->URL=ed->Text;
}
//---------------------------------------------------------------------------
void __fastcall TFm::edKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift)
{
	if (Key == vkReturn) {
		wb->URL = ed->Text;
	}
}
//---------------------------------------------------------------------------
void __fastcall TFm::wbDidFinishLoad(TObject *ASender)
{
	ed->Text=wb->URL;
}
//---------------------------------------------------------------------------
