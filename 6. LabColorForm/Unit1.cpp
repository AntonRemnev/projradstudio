//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void Tfm::NewColor(TAlphaColor aValue)
{
	laValue->Text=AlphaColorToString(aValue);
	laRGB->Text=Format("(%d,%d,%d)",
	ARRAYOFCONST((
		TAlphaColorRec(aValue).R,
		TAlphaColorRec(aValue).G,
		TAlphaColorRec(aValue).B))
	);
	fm->Fill->Color=aValue;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buRandomClick(TObject *Sender)
{
	NewColor(TAlphaColorF::Create(Random(256),Random(256),Random(256)).ToAlphaColor());
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buClick(TObject *Sender)
{
	NewColor(TAlphaColor(TAlphaColorRec::Lightgreen));
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormCreate(TObject *Sender)
{
	Randomize;
	this->Fill->Kind=TBrushKind::Solid;
	buRandomClick(buRandom);
}
//---------------------------------------------------------------------------

