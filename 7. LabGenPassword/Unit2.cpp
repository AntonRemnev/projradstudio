//---------------------------------------------------------------------------

#pragma hdrstop

#include "Unit2.h"
#include <System.Classes.hpp>
//---------------------------------------------------------------------------
#pragma package(smart_init)
System::UnicodeString RandomStr(int Length, bool ckDown, bool ckUp, bool
ckNumber, bool ckSpec)
{
	const char *c1="qwertyuiopasdfghjklzxcvbnm";
	const char *c2="0123456789";
	const char *c3="!@#$%^&*()_=-+[]{}/\<>?�";
	//
	System::UnicodeString x="";
	System::UnicodeString xResult="";
	if (ckDown)x+=c1;
	if (ckUp)x+=UpperCase(c1);
	if (ckNumber) x+=c2;
	if (ckSpec) x+= c3;
	if (x.IsEmpty()) x= c1;
	//
	while(xResult.Length() < Length)
		(xResult += x.SubString(Random(x.Length()+1),1));
	return xResult;
}
