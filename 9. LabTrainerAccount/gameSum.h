//---------------------------------------------------------------------------

#ifndef gameSumH
#define gameSumH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TLayout *Layout1;
	TButton *buReload;
	TButton *buInfo;
	TGridPanelLayout *GridPanelLayout1;
	TLabel *laSum;
	TLabel *Label4;
	TGridPanelLayout *GridPanelLayout2;
	TRectangle *Rectangle3;
	TLabel *laCountCorrect;
	TRectangle *Rectangle4;
	TLabel *laCountWrong;
	TButton *buYes;
	TButton *buNo;
	void __fastcall buReloadClick(TObject *Sender);
	void __fastcall buYesClick(TObject *Sender);
	void __fastcall buNoClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buInfoClick(TObject *Sender);
private:	// User declarations
	bool FAnswerCorrect;
	int FCountCorrect;
	int FCountWrong;
	void DoAnswer(bool answer);
	void generateSum();
	void DoContinue();
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
