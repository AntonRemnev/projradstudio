// ---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "gameSum.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;

// ---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner) : TForm(Owner) {
}

// ---------------------------------------------------------------------------
void TForm1::generateSum() {

	laCountCorrect->Text = Format(L"����� %d", ARRAYOFCONST((FCountCorrect)));
	laCountWrong->Text = Format(L"������� %d", ARRAYOFCONST((FCountWrong)));
	int firstNum = random(20) + 1;
	int secondNum = random(20) + 1;
	int answer = firstNum + secondNum;

    int xSing = (Random(2)) ? 1 : -1;
	int newAnswer = (Random(2) == 1) ? answer : answer + Random(7) * xSing;

	FAnswerCorrect = (answer == newAnswer);

	laSum->Text = Format("%d + %d = %d",
	ARRAYOFCONST((firstNum, secondNum, newAnswer)));

}

void TForm1::DoAnswer(bool answer){
	if(FAnswerCorrect == answer)
		FCountCorrect++;
	else
		FCountWrong++;
    generateSum();
}



void __fastcall TForm1::buReloadClick(TObject *Sender) {
	FCountCorrect = 0;
    FCountWrong = 0;
	generateSum();
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::buYesClick(TObject *Sender)
{
    DoAnswer(true);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buNoClick(TObject *Sender)
{
    DoAnswer(false);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormCreate(TObject *Sender)
{
	Randomize();
    generateSum();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buInfoClick(TObject *Sender)
{
    ShowMessage("���������� ��������� 151-364");
}
//---------------------------------------------------------------------------

