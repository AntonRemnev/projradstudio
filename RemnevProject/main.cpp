//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TFm1 *Fm1;
//---------------------------------------------------------------------------
__fastcall TFm1::TFm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFm1::FormCreate(TObject *Sender)
{
    Randomize(); // ������� ��� ����������� ������������ Random()
	DoReset();   // �������� ����������
	tc->ActiveTab = tiMain;  // ��������� �� ������� �������
    combList = new int[20]; // ������ ��� ����������
	rectList = new TList;   // ������ ��� ���������

	for(int i = 1; i <= 9; i++){
		rectList->Add(this->FindComponent("Rectangle"+IntToStr(i))); // ���������� � ������ ��� ��������  1-9
	}
}
//---------------------------------------------------------------------------

void TFm1::GenerateComb(){
    int choose = Random(9)+1; // ���������� ��������� ����� �� 1 �� 9
	combList[Complexity] = choose; // ��������� ����� � ��� ������ � �����������
	Complexity++; // �������� ��������� �� 1
	ShowComb(); // ��������� �������� ����������� ����������
}
//---------------------------------------------------------------------------

void __fastcall TFm1::onAllRectangleClick(TObject *Sender)
{
	// ���� �� ����� �������� �� ��������
	if(canTouch){
		TRectangle* rect = (TRectangle*)Sender;
		rect->Opacity = 0; // ������� ������������
		// ��������������� ������������ � ������� �������� �� 1 �������
		TAnimator::AnimateFloatWait(rect, "Opacity", 1, 1,TAnimationType::Out, TInterpolationType::Linear); // �������� �������
		DoChoise(rect->Tag);  // ������� ��� �������� � ����� DoChoise
	}
}
//---------------------------------------------------------------------------


void TFm1::DoChoise(int aTag){
	//
	if(combList[FStep] == aTag){  // ���� ������������������ ����������, �� ����������
		DoContinue();
	}else{ // ����� ��������� ����
		laEndScore->Text = L"��� ����: " + IntToStr(FScore);   // ������ ���������� ������������ ����� �� ������� � �����������
		tc->GotoVisibleTab(tiResult->Index);  // ������ ��������� �� ������� � �����������
	}
}


void TFm1::DoContinue(){
	FStep++;  // ��������� 1 ���
	if(Complexity <= FStep){  // ���� ��������� ������ ��� ����� �������� ����
		GenerateComb(); // ��������� ����� ����� � ����������
		FScore += FStep*10; // ����������� ����
        laScore->Text = L"����: " + IntToStr(FScore);  // ������ label �� ������
		FStep = 0; // �������� ��� ���, ����� �� ����� ������ � ������ ����������
	}
}


void TFm1::ShowComb(){
	ShowStep = 0; // �������� � ������ ����������
	tiShow->Enabled = true; // �������� ������ ��� �������� ����������� ����������
	canTouch = false; // ��������� �������� �� ��������, ���� ��� ��������
    laInstruction->Text = L"���������!"; // ������ ��������� ��� ������
}
void __fastcall TFm1::buPlayClick(TObject *Sender)
{
	DoReset(); // �������� ����������
	tc->GotoVisibleTab(tiPlay->Index);  // ������ ��������� �� ������� � �����
	GenerateComb();  // ���������� ����������
}
//---------------------------------------------------------------------------

void __fastcall TFm1::tiShowTimer(TObject *Sender)
{
	if(ShowStep < Complexity){ // ���� ���������� �������� �� ���������
		((TRectangle*)rectList->Items[combList[ShowStep]-1])->Opacity = 0; // ������� ������������
        // ��������������� ������������ � ������� �������� �� 1 �������
		TAnimator::AnimateFloat((TRectangle*)rectList->Items[combList[ShowStep]-1], "Opacity", 1, 1,TAnimationType::Out, TInterpolationType::Linear);
        ShowStep++; // ����������� ��� ���������� �� 1
	}else{ // ���� ��� ���������� ��������
		tiShow->Enabled = false; // ������������� ������ ����������� ����������
		canTouch = true; // ��������� �������� �� ��������
		laInstruction->Text = L"��������!"; // ������ ��������� ��� ������������
    }
}
//---------------------------------------------------------------------------

void TFm1::DoReset(){
	FScore = 0;  // ��������� �����
	FStep = 0;  // ������� ���, �� ������� ������������
	ShowStep = 0;  // ��� ����������� ����������
	Complexity = 0; // ��������� - ������� ��������� � ����������
	canTouch = false; // ��������� �������� �� ��������
	laInstruction->Text = L"���������!"; // ������ ��������� ��� ������������
	laScore->Text = L"����: " + IntToStr(FScore);  // ������ label �� ������
}
void __fastcall TFm1::buRestartClick(TObject *Sender)
{
	  buPlayClick(Sender); // ��������� ������� ������ buPlay
}
//---------------------------------------------------------------------------


void __fastcall TFm1::buBackClick(TObject *Sender)
{
    tc->GotoVisibleTab(tiMain->Index);  // ������ ��������� � ������� ����
}
//---------------------------------------------------------------------------

void __fastcall TFm1::FormDestroy(TObject *Sender)
{
	delete combList; // ����������� ������
	delete rectList; // ����������� ������
}
//---------------------------------------------------------------------------

void __fastcall TFm1::buAboutClick(TObject *Sender)
{
    tc->GotoVisibleTab(tiAbout->Index); // ������ ��������� �� ������� � ���������
}
//---------------------------------------------------------------------------

void __fastcall TFm1::buInfoClick(TObject *Sender)
{
    ShowMessage(L"�����������: ������ ����� \n������: 151-364 \n���������� ��������������� ����������� \n2016 ���");
}
//---------------------------------------------------------------------------

void __fastcall TFm1::allMouseEnter(TObject *Sender)
{
	// ������ HotButton. ��� ��������� ������������� ����� � ����������� ������� ������, ��������� ������ �����
	TButton *x = ((TButton*)Sender);
	x->TextSettings->Font->Size += 5;
	x->Margins->Rect = TRect(0,0,0,0);
	x->TextSettings->Font->Style = x->TextSettings->Font->Style << TFontStyle::fsBold;
}
//---------------------------------------------------------------------------

void __fastcall TFm1::allMouseLeave(TObject *Sender)
{
    // ������� HotButton. ���� ����� �� �� ������, �� ��������� ����� � ����������� ������� ������, ������� ������ �����
	TButton *x = ((TButton*)Sender);
	x->TextSettings->Font->Size -= 5;
	x->Margins->Rect = TRect(5,5,5,5);
	x->TextSettings->Font->Style = x->TextSettings->Font->Style >> TFontStyle::fsBold;
}
//---------------------------------------------------------------------------


void __fastcall TFm1::onAllMouseEnter(TObject *Sender)
{
   //
   if(canTouch){
   TRectangle *x = ((TRectangle *)Sender);
   x->Opacity = 0.7;
   }
}
//---------------------------------------------------------------------------

void __fastcall TFm1::onAllMouseLeave(TObject *Sender)
{
//
if(canTouch){
	TRectangle *x = ((TRectangle *)Sender);
   x->Opacity = 1;
}
}
//---------------------------------------------------------------------------

