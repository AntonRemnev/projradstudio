//---------------------------------------------------------------------------

#ifndef mainH
#define mainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.Ani.hpp>
//---------------------------------------------------------------------------
class TFm1 : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiMain;
	TTabItem *tiPlay;
	TTabItem *tiAbout;
	TLayout *Layout1;
	TGridPanelLayout *gpGame;
	TRectangle *Rectangle1;
	TRectangle *Rectangle2;
	TRectangle *Rectangle3;
	TRectangle *Rectangle4;
	TRectangle *Rectangle5;
	TRectangle *Rectangle6;
	TRectangle *Rectangle7;
	TRectangle *Rectangle8;
	TRectangle *Rectangle9;
	TToolBar *ToolBar1;
	TButton *buBack;
	TLabel *laScore;
	TLabel *laInstruction;
	TGridPanelLayout *GridPanelLayout2;
	TButton *buPlay;
	TButton *buAbout;
	TTimer *tiShow;
	TTabItem *tiResult;
	TLayout *Layout2;
	TLabel *Label2;
	TLabel *laEndScore;
	TButton *buRestart;
	TButton *buGoToMenu;
	TFloatAnimation *FloatAnimation1;
	TFloatAnimation *FloatAnimation2;
	TLabel *laName;
	TFloatAnimation *FloatAnimation3;
	TText *Text1;
	TToolBar *ToolBar2;
	TButton *buBack2;
	TButton *buInfo;
	TGridPanelLayout *GridPanelLayout1;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall onAllRectangleClick(TObject *Sender);
	void __fastcall buPlayClick(TObject *Sender);
	void __fastcall tiShowTimer(TObject *Sender);
	void __fastcall buRestartClick(TObject *Sender);
	void __fastcall buBackClick(TObject *Sender);
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall buAboutClick(TObject *Sender);
	void __fastcall buInfoClick(TObject *Sender);
	void __fastcall allMouseEnter(TObject *Sender);
	void __fastcall allMouseLeave(TObject *Sender);
	void __fastcall onAllMouseEnter(TObject *Sender);
	void __fastcall onAllMouseLeave(TObject *Sender);
private:	// User declarations
	int FScore;
	int FStep;
	int ShowStep;
	int Complexity;
	TList *rectList;
	int *combList;
    bool canTouch;

	void DoReset();
	void DoChoise(int aTag);
	void ShowComb();
	void DoContinue();
    void GenerateComb();
public:		// User declarations
	__fastcall TFm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFm1 *Fm1;
//---------------------------------------------------------------------------
#endif
