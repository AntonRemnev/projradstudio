//---------------------------------------------------------------------------

#ifndef FirstH
#define FirstH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TFM : public TForm
{
__published:	// IDE-managed Components
	TButton *buClick;
	void __fastcall buClickClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TFM(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFM *FM;
//---------------------------------------------------------------------------
#endif
