//---------------------------------------------------------------------------

#ifndef pr1H
#define pr1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Objects.hpp>
#include <System.Math.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *buReset;
	TButton *buAbout;
	TTimer *tiRemember;
	TLayout *ly;
	TRectangle *Rectangle1;
	TRectangle *Rectangle2;
	TRectangle *reRemember;
	TLabel *laWrong;
	TLabel *laCorrect;
	TLabel *laRemember;
	TLabel *laNumber;
	TButton *buAnswer;
	TEdit *edAnswer;
	TLabel *laAnswer;
	TProgressBar *pbRemember;
	TButton *buPlus;
	TButton *buMinus;
	TStyleBook *StyleBook1;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buResetClick(TObject *Sender);
	void __fastcall tiRememberTimer(TObject *Sender);
	void __fastcall buAnswerClick(TObject *Sender);
	void __fastcall edAnswerKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
		  TShiftState Shift);
	void __fastcall buPlusClick(TObject *Sender);
	void __fastcall buMinusClick(TObject *Sender);
	void __fastcall buAboutClick(TObject *Sender);
private:	// User declarations
int FCountCorrect;
int FCountWrong;
int FSecretNumber;
void DoReset();
void DoContinue();
void DoAnswer();
void DoViewQuestion(bool aValue);
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
