//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "pr1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TForm1::DoReset()
{
	FCountCorrect=0;
	FCountWrong=0;
	DoContinue();
}
//---------------------------------------------------------------------------
void TForm1::DoContinue()
{
	laCorrect->Text=L"����� " +IntToStr(FCountCorrect);
	laWrong->Text=L"������� " + IntToStr(FCountWrong);
	FSecretNumber=RandomRange(100000, 999999);
	laNumber->Text=IntToStr(FSecretNumber);
	edAnswer->Text=" ";
	pbRemember->Value=0;
	DoViewQuestion(true);
}
//---------------------------------------------------------------------------
void TForm1::DoAnswer()
{
	int x=StrToIntDef(edAnswer->Text, 0);
	if (x==FSecretNumber) {FCountCorrect++;
	}
		else {FCountWrong++;
		};
	DoContinue();
}
//---------------------------------------------------------------------------
void TForm1::DoViewQuestion(bool aValue)
{
	tiRemember->Enabled=aValue;
	laRemember->Visible=aValue;
	reRemember->Visible=aValue;
	pbRemember->Visible=aValue;
	laAnswer->Visible= ! aValue;
	edAnswer->Visible= ! aValue;
	buAnswer->Visible= ! aValue;
	if (edAnswer->Visible) {
		edAnswer->SetFocus();
		}
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
	Randomize();
	DoReset();
}

//---------------------------------------------------------------------------
void __fastcall TForm1::buResetClick(TObject *Sender)
{
	DoReset();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::tiRememberTimer(TObject *Sender)
{
	pbRemember->Value+=1;
	if (pbRemember-> Value>= pbRemember->Max){
		 DoViewQuestion(false);
	}
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buAnswerClick(TObject *Sender)
{
DoAnswer();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::edAnswerKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift)
{
	if (Key == vkReturn) {
		DoAnswer();
	}
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buPlusClick(TObject *Sender)
{
	ly->Scale->X+=0.1;
	ly->Scale->Y+=0.1;
	ly->RecalcSize();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buMinusClick(TObject *Sender)
{
	ly->Scale->X-=0.1;
	ly->Scale->Y-=0.1;
	ly->RecalcSize();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buAboutClick(TObject *Sender)
{
	ShowMessage("�����������: ������ �����(151-364)");
}
//---------------------------------------------------------------------------

