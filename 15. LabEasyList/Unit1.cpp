//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
//const UnicodeString cList[]= {
	//"couple","hardtop","hatcback","liftback","limousine","minibus",
	//"minivan", "sedan","universal"};
//---------------------------------------------------------------------------
void TForm1::LoadItem(int aIndex)
{
	UnicodeString xName = System::Ioutils::TPath::Combine(FPath, cList[aIndex]);
	//
	Image1->Bitmap->LoadFromFile(xName+".jpg");
	//
	TStringList *x = new TStringList;
	__try {
		x->LoadFromFile(xName+".txt");
		tx->Text=x->Text;
	}
	__finally {
        x->DisposeOf();
    }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ListBox1ItemClick(TCustomListBox * const Sender, TListBoxItem * const Item)

{
	sb->ViewportPosition=TPointF(0,0);
	LoadItem(Item->Index);
	tx->RecalcSize();
	tc->ActiveTab=TiIcon;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
	tc->ActiveTab=TiList;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
	FPath=
	#ifdef _Windows
	"..\\..\\inc\\";
	#else
	System::Ioutils::TPath::GetDocumentsPath();
	#endif
	//
	tc->ActiveTab=TiList;
}
//---------------------------------------------------------------------------
