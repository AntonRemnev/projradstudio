//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Ani.hpp>
#include <FMX.ListBox.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <System.IOUtils.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tcMain;
	TTabItem *tiMain;
	TButton *buPlay;
	TButton *buAuthor;
	TButton *buHelp;
	TButton *buExit;
	TTabItem *tiLVL;
	TLabel *Label1;
	TImage *Image11;
	TLayout *Layout1;
	TTabItem *tiASIA;
	TTabItem *tiEUROPE;
	TTabItem *tiAFRICA;
	TTabItem *tiAMERICA1;
	TTabItem *tiAUSTRALIA;
	TImage *Image12;
	TImage *Image13;
	TImage *Image14;
	TImage *Image15;
	TImage *Image16;
	TGridPanelLayout *GridPanelLayout2;
	TLabel *laRus11;
	TLabel *laChina12;
	TLabel *laYaponia13;
	TLabel *laIndia14;
	TLabel *laIran15;
	TLabel *laFilippini16;
	TGridPanelLayout *GridPanelLayout3;
	TLabel *laFrance18;
	TLabel *laItaly19;
	TLabel *laIspania20;
	TLabel *laRuminia21;
	TLabel *laPolish22;
	TGridPanelLayout *gp;
	TLabel *laMorokko25;
	TLabel *laMadagaskar26;
	TLabel *laEgyph27;
	TLabel *laAngola28;
	TLabel *laNiger29;
	TGridPanelLayout *GridPanelLayout5;
	TLabel *laArgentina;
	TLabel *laBrazil;
	TLabel *laYragvay;
	TLabel *laParagvay;
	TLabel *laEkvador;
	TLabel *laTrinidad;
	TGridPanelLayout *GridPanelLayout6;
	TLabel *laAustralia;
	TTabItem *tiFINISH;
	TLabel *Label46;
	TButton *Button1;
	TLabel *laTime;
	TTabItem *tiAMERICA2;
	TGridPanelLayout *GridPanelLayout7;
	TLabel *laGrenlandia;
	TLabel *laKanada;
	TLabel *laMexica;
	TLabel *laKuba;
	TImage *Image17;
	TButton *buClose;
	TButton *Button10;
	TButton *Button11;
	TButton *Button8;
	TButton *Button7;
	TButton *Button6;
	TButton *Button5;
	TLayout *layAmerica2;
	TLayout *layAmerica1;
	TLayout *layAustralia;
	TLayout *layAfrica;
	TLayout *layEurope;
	TLayout *layASIA;
	TLayout *layMorokko;
	TLayout *layMadagaskar;
	TLayout *layAustra1ia;
	TLayout *layBrazil;
	TLayout *layYragvay;
	TLayout *layTrinidad;
	TLayout *layArgentina;
	TLayout *layParagvay;
	TLayout *layEkvador;
	TLayout *layEgyph;
	TLayout *layAngola;
	TLayout *layNiger;
	TLabel *laTasmania;
	TLabel *laKvislend;
	TLayout *layKvislend;
	TLayout *layTasmania;
	TLayout *layGrenlandia;
	TLayout *layKanada;
	TLayout *layMexica;
	TLayout *layKuba;
	TLayout *layRus;
	TLayout *layFilippini;
	TLayout *laChina;
	TLayout *layYaponia;
	TLayout *layIndia;
	TLayout *layIran;
	TLayout *laFrance;
	TLayout *layItaly;
	TLayout *layIspania;
	TLayout *layPolish;
	TLayout *layRuminia;
	TStyleBook *StyleBook1;
	TColorAnimation *ColorAnimation1;
	TTimer *WinStrick;
	TTimer *GameTimer;
	TButton *Button2;
	TTabItem *tiHELP;
	TListBox *lb;
	TListBoxItem *ListBoxItem1;
	TListBoxItem *ListBoxItem2;
	TListBoxItem *ListBoxItem3;
	TListBoxItem *ListBoxItem4;
	TListBoxItem *ListBoxItem5;
	TListBoxItem *ListBoxItem6;
	TLayout *Layout2;
	TImage *im;
	TButton *buNext;
	TText *tx;
	TLayout *ly;
	void __fastcall buAuthorClick(TObject *Sender);
	void __fastcall layRusClick(TObject *Sender);
	void __fastcall laChinaClick(TObject *Sender);
	void __fastcall layYaponiaClick(TObject *Sender);
	void __fastcall layIndiaClick(TObject *Sender);
	void __fastcall layIranClick(TObject *Sender);
	void __fastcall layFilippiniClick(TObject *Sender);
	void __fastcall layItalyClick(TObject *Sender);
	void __fastcall laFranceClick(TObject *Sender);
	void __fastcall layIspaniaClick(TObject *Sender);
	void __fastcall layRuminiaClick(TObject *Sender);
	void __fastcall layPolishClick(TObject *Sender);
	void __fastcall layMorokkoClick(TObject *Sender);
	void __fastcall layMadagaskarClick(TObject *Sender);
	void __fastcall layEgyphClick(TObject *Sender);
	void __fastcall layAngolaClick(TObject *Sender);
	void __fastcall layNigerClick(TObject *Sender);
	void __fastcall layArgentinaClick(TObject *Sender);
	void __fastcall layBrazilClick(TObject *Sender);
	void __fastcall layYragvayClick(TObject *Sender);
	void __fastcall layTrinidadClick(TObject *Sender);
	void __fastcall layParagvayClick(TObject *Sender);
	void __fastcall layEkvadorClick(TObject *Sender);
	void __fastcall layAustra1iaClick(TObject *Sender);
	void __fastcall layTasmaniaClick(TObject *Sender);
	void __fastcall layKvislendClick(TObject *Sender);
	void __fastcall layGrenlandiaClick(TObject *Sender);
	void __fastcall layKanadaClick(TObject *Sender);
	void __fastcall layMexicaClick(TObject *Sender);
	void __fastcall layKubaClick(TObject *Sender);
	void __fastcall layAfricaClick(TObject *Sender);
	void __fastcall layASIAClick(TObject *Sender);
	void __fastcall layEuropeClick(TObject *Sender);
	void __fastcall layAmerica1Click(TObject *Sender);
	void __fastcall layAmerica2Click(TObject *Sender);
	void __fastcall layAustraliaClick(TObject *Sender);
	void __fastcall buPlayClick(TObject *Sender);
	void __fastcall buExitClick(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall WinStrickTimer(TObject *Sender);
	void __fastcall GameTimerTimer(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall gpmainClick(TObject *Sender);
	void __fastcall lbItemClick(TCustomListBox * const Sender, TListBoxItem * const Item);
	void __fastcall buNextClick(TObject *Sender);
	void __fastcall buHelpClick(TObject *Sender);

private:
	int a, b, c, d, e, f;
	int Aa, Ab, Ac, Ad, Ae, Af;
	int Ea, Eb, Ec, Ed, Ee;
	int AFa, AFb, AFc, AFd, AFe;
	int AMa, AMb, AMc, AMd, AMe, AMf;
	int AUa, AUb, AUc;
	int AMMa, AMMb, AMMc, AMMd, AMMe;
	TDateTime  FDataTimer;
	UnicodeString FPath;
	void LoadItem(int aIndex);
	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
	const UnicodeString cList[]= {
	"Asia","Africa","Eur","Aust","NAmerica","SAmerica"};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
