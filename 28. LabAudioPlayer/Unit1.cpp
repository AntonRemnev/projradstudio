//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
#include "Unit2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TForm1::DoRefreshList(UnicodeString aPath)
{
	laPath-> Text=aPath;
	DynamicArray<UnicodeString> x;
	TListViewItem *xItem;
	x=System::Ioutils::TDirectory::GetFiles(aPath,"*.mp3"); //
	lv->BeginUpdate();
	try {
		for (int i=0; i<x.Length; i++) {
			xItem=lv->Items->Add();
			xItem->Text=System::Ioutils::TPath::GetFileNameWithoutExtension(x[i]);
			xItem->Detail=x[i];
		}
	}
	__finally {
		lv->EndUpdate();
	}
}
//---------------------------------------------------------------------------
void TForm1::DoStop()
{
	mp->Stop();
	tm->Enabled= false;
	lv->ItemIndex= -1;
	tbCurrentTime->Value=0;
	FPauseTime=0;
}

void __fastcall TForm1::lvItemClick(TObject * const Sender, TListViewItem * const AItem)

{
	buPlayClick(this);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormCreate(TObject *Sender)
{
    FPauseTime=0;
	DoRefreshList(System::Ioutils::TPath::GetSharedMusicPath());
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buPlayClick(TObject *Sender)
{
	if (lv->ItemIndex!=-1) {
		tm->Enabled=false;
		mp->Clear();
		mp->FileName= lv->Items->AppearanceItem[lv->ItemIndex]->Detail;
		tbCurrentTime->Enabled=true;
		tbCurrentTime->Max= mp->Duration;
		laDuration->Text=Format("%2.2d:%2.2d", ARRAYOFCONST((
			(unsigned int)mp->Duration / MediaTimeScale / 60
			,(unsigned int)mp->Duration / MediaTimeScale % 60
			)));
			mp->CurrentTime=FPauseTime;
			FPauseTime=0;
			mp->Play();
			tm->Enabled=true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buPauseClick(TObject *Sender)
{
	if (mp->State == TMediaState::Playing){
		FPauseTime=mp->Media->CurrentTime;
		mp->Stop();
		} else {
			buPlayClick(this);
			}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buStopClick(TObject *Sender)
{
	if (mp-> State == TMediaState::Playing) {
		DoStop();
	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::tbCurrentTimeChange(TObject *Sender)
{
	if (tbCurrentTime->Tag == 0){
	//
	if ((mp->State== TMediaState::Stopped) && (FPauseTime !=0)) {
		FPauseTime =tbCurrentTime->Value;
		} else {
		mp->CurrentTime=tbCurrentTime->Value;
		}

	}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buPrevClick(TObject *Sender)
{
	if ((lv->ItemIndex != -1) && (lv->ItemIndex >0)){
		lv->ItemIndex =lv->ItemIndex -1;
		FPauseTime =0;
		buPlayClick(this);
		} else {
			DoStop();
		}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buNextClick(TObject *Sender)
{
	if ((lv->ItemIndex != -1) && (lv->ItemIndex < lv->ItemCount-1)){
		lv->ItemIndex =lv->ItemIndex +1;
		FPauseTime =0;
		buPlayClick(this);
		} else {
			DoStop();
		}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::tmTimer(TObject *Sender)
{
	 tbCurrentTime->Tag = 1;
	 tbCurrentTime->Value=mp->CurrentTime;
	 tbCurrentTime->Tag = 0;
	 laCurrentTime->Text = Format("%2.2d:%2.2d", ARRAYOFCONST((
			(unsigned int)mp->CurrentTime / MediaTimeScale / 60
			,(unsigned int)mp->CurrentTime / MediaTimeScale % 60
			)));
			//
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buAboutClick(TObject *Sender)
{
	ShowMessage("�����������: ���������� ���������(151-364)");
}
//---------------------------------------------------------------------------

