//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <FMX.Media.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <System.IOUtils.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TToolBar *tb;
	TLabel *laPath;
	TListView *lv;
	TMediaPlayer *mp;
	TTimer *tm;
	TLayout *ly1;
	TLayout *ly2;
	TTrackBar *tbCurrentTime;
	TLabel *laDuration;
	TLabel *laCurrentTime;
	TButton *buPlay;
	TButton *buStop;
	TButton *buPause;
	TButton *buNext;
	TButton *buPrev;
	TLabel *laName;
	TButton *buAbout;
	TStyleBook *StyleBook1;
	void __fastcall lvItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buPlayClick(TObject *Sender);
	void __fastcall buPauseClick(TObject *Sender);
	void __fastcall buStopClick(TObject *Sender);
	void __fastcall tbCurrentTimeChange(TObject *Sender);
	void __fastcall buPrevClick(TObject *Sender);
	void __fastcall buNextClick(TObject *Sender);
	void __fastcall tmTimer(TObject *Sender);
	void __fastcall buAboutClick(TObject *Sender);

private:	// User declarations
int FPauseTime;
void DoRefreshList(UnicodeString aPath);
void DoStop();
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
